#!/bin/bash
#
# Create header and footer template files for the Fellowship Account Management System
#
# Headers and footers are extracted from the /fellowship/index.xx.html files,
# based on markers generated by /fellowship/default.xsl

# Directory where the /fellowship html files are written by the build process
#
# We assume that the script is called by build.sh passing $1 as the $DEST
# directory defined in build.sh (so that we can reuse this script in both production
# and test instances)
if [ -z "$1" ] ; then
  echo "You must supply a DEST directory"
  exit 1
else
  FELLDIR=$1/global/fellowship
fi

# Directory where the template files are expected by the AMS
TPLDIR=$FELLDIR/tpl
mkdir -p $TPLDIR

# Temporary fix, until we re-enable automated generation of templates
cp -a /var/www/fellowship-ams/tpl/* $TPLDIR
chown -R www: $TPLDIR
exit 0


# Working directory for this script
WRKDIR=`mktemp -d`

cp $FELLDIR/index.*.html $WRKDIR

cd $WRKDIR

for i in `ls index.*.html` ; do
  # Extract language code from file name
  LANGUAGE=`echo $i | cut -d '.' -f 2`

  # Split and save to h1xx
  csplit -f h1 -s $i '%<!--Start-fellowship-header-->%'
  # Split h100 and save to h2xx => h200 is our header template
  csplit -f h2 -s h100 '/<!--Stop-fellowship-header-->/1'
  mv h200 $TPLDIR/tpl_header.$LANGUAGE.html
  
  # Split and save to f1xx => f100 is our footer template
  csplit -f f1 -s $i '%<!--Start-fellowship-footer-->%-1'
  mv f100 $TPLDIR/tpl_footer.$LANGUAGE.html
done

rm -rf $WRKDIR
